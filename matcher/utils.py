import json
import queue
from urllib.parse import quote
import requests

from nltk.tree import *

parser_host = 'http://localhost:9000/'
parser_option = "{'annotators': 'parse', 'outputFormat': 'json'}"
parser_option_encoded = quote(parser_option)
parser_url = parser_host + "?properties=" + parser_option_encoded


def make_tree(sentence):
    """
    Run a constituency parser given sentence, and return all the subtrees
    :param sentence: string
    :return: nltk tree
    """
    resp = requests.post(parser_url, data=sentence.encode('utf-8'))
    resp.encoding = 'utf-8'
    parsed_resp = json.loads(resp.text.replace('\u0000', ''), strict=False)
    tree_str = parsed_resp['sentences'][0]['parse']
    tree = Tree.fromstring(tree_str)

    return tree


def traverse_bfs(tree):
    """
    Traverse NLTK tree in BFS order
    :param tree: nltk tree
    :return: A generator yielding subtrees while traversing
    """

    q = queue.Queue()
    q.put(tree)

    while not q.empty():
        current_tree = q.get()
        if isinstance(current_tree, Tree):
            for child in current_tree:
                if isinstance(child, Tree):
                    q.put(child)
        yield current_tree
