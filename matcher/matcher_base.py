import json
from collections import namedtuple
import logging

import nltk
from nltk.corpus import stopwords
from nltk.corpus import wordnet as wn
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer


class BaseMatcher:
    Reference = namedtuple('Reference', ['slide_id', 'word_id', 'subtitle_id'])
    logger = logging.getLogger('default logger').setLevel(logging.INFO)

    class EmptyToken(Exception):
        pass

    @staticmethod
    def log_slide(logger, slide, subtitles):
        logger.debug("======================")
        logger.debug("Slide{}".format(slide['index']))
        logger.debug("Words are ...")
        for word in slide['words']:
            logger.debug("\t {} '{}'".format(word['id'], word['text']))
        logger.debug("Subtitles are...")
        for subtitle in subtitles:
            logger.debug("\t {} '{}'".format(subtitle['id'], " ".join(subtitle['lines'])))

    @staticmethod
    def open_slides(slides_path):
        with open(slides_path, 'r') as f:
            slides = json.load(f)
        return slides

    @staticmethod
    def open_subtitle(subtitle_path):
        with open(subtitle_path, 'r') as f:
            subtitle = json.load(f)
        return subtitle

    @staticmethod
    def tokenize(sentence):
        lowered = sentence.lower()
        tokens = nltk.word_tokenize(lowered)
        tokens_split = []
        for token in tokens:
            tokens_split += token.split('-')
        # Remove punctuation
        tokens_wo_punctuation = list(filter(lambda w: w.isalpha(), tokens_split))
        # Remove stop words
        stopwords_set = set(stopwords.words('english'))
        tokens_wo_stopwords = list(filter(lambda w: w not in stopwords_set, tokens_wo_punctuation))

        # Lemmatize
        wnl = WordNetLemmatizer()
        lemmatized = [wnl.lemmatize(word) for word in tokens_wo_stopwords]

        return lemmatized

    @staticmethod
    def stemmize(sentence):
        """
        Convert string into set of token words by remove stop words, stem, etc...
        :param sentence: string
        :return: list of string
        """
        tokens = BaseMatcher.tokenize(sentence)
        # Stemmize
        stemmer = PorterStemmer()
        stemmed = [stemmer.stem(token) for token in tokens]

        return stemmed

    @staticmethod
    def populate_synonyms(word):
        """
        Find similar words in synset
        :param word: string
        :return: list of string
        """
        synset = wn.synset(word)
        synonyms = [w for w in synset[0].lemma_names() if w != word] if len(synset) >= 1 else []
        return synonyms

    @staticmethod
    def subtitle_for_slide(slide, subtitles):
        """
        Return subset of subtitles within given time range
        :param slide: slide dict
        :param subtitles: list of subtitle dict
        :return: list of subtitle
        """
        assert 'start' in slide, "Slide is in wrong format. {}".format(slide)
        assert 'end' in slide, "Slide is in wrong format. {}".format(slide)

        start, end = slide['start'], slide['end']
        return [subtitle for subtitle in subtitles
                if subtitle['start'] >= start and subtitle['end'] < end]

    @staticmethod
    def contains(words_a, words_b):
        """
        Check if 'words_b ⊂ words_a' (except for b is empty)
        :param words_a: list of words
        :param words_b: list of words
        :return: boolean
        """
        if len(words_b) == 0:
            return False

        for word in words_b:
            if word not in words_a:
                return False
        return True

    @staticmethod
    def list_partition(l, num_partition):
        """
        Return
        :param l: any list
        :param num_partition:
        :return:
        """

    @staticmethod
    def reference_nodup(references):
        reference_set = set(references)
        return reference_set

    @staticmethod


    def match(self):
        raise NotImplementedError
