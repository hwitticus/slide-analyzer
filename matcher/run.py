from matcher import BaseMatcher, SyntacticMatcher, SemanticNaiveMatcher, SemanticSyntacticMatcher, SemanticOrderedMatcher, PermutationMatcher
from collections import namedtuple
import logging
import json
import sys
import os
import time


BASE_PATH = os.path.abspath(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

class Runner:
    MatcherInfo = namedtuple('MatcherInfo', ['matcher', 'description'])
    matchers = {
        'syntactic': MatcherInfo(SyntacticMatcher, 'Search only for exact inclusion'),
        'semantic_naive': MatcherInfo(SemanticNaiveMatcher, 'Search using word embedding distance'),
        'semantic_syntactic': MatcherInfo(SemanticSyntacticMatcher, 'Search using word distance, but with subtrees'),
        'semantic_ordered': MatcherInfo(SemanticOrderedMatcher,
                                        "Search using word distance, within subtree, with order constraint"),
        'semantic_permutation': MatcherInfo(PermutationMatcher, "Search best order from whole orderings")
    }

    def __init__(self, matcher_name, verbose=False, matcher_options={}):
        assert matcher_name in self.matchers, '{} is not valid matcher name. Valid options: {}'.format(matcher_name, self.matchers)
        self.matcher_name = matcher_name
        self.matcher_options = matcher_options
        self.results = []
        self.verbose = verbose
        self.references = []
        self.logger = self.get_logger()

    def run(self, name, slides_path, subtitle_path, answer_path):
        self.logger.debug("New case {}".format(name))
        answers = Runner.read_answer(answer_path)

        matcher = self.matchers[self.matcher_name].matcher(slides_path, subtitle_path, self.logger)
        references = matcher.match()
        # references = matcher.reference_nodup(references)

        self.references += references
        result = self.evaluate(references, answers)
        if self.verbose:
            self.logger.info(json.dumps({
                'name': name,
                'total': result[0],
                'correct': result[1],
                'accuracy': (result[1] / result[0]) * 100
            }, indent=2))
        self.results.append(result)

    def run_case(self, c):
        self.run(c['name'], c['slides_path'], c['subtitle_path'], c['answer_path'])

    def run_cases(self, case_list):
        for c in case_list:
            self.run(c['name'], c['slides_path'], c['subtitle_path'], c['answer_path'])

    def report_total(self):
        total, correct = 0, 0
        for result in self.results:
            total += result[0]
            correct += result[1]
        self.logger.info(json.dumps({
            'name': 'TOTAL using {}'.format(self.matcher_name),
            'total': total,
            'correct': correct,
            'accuracy': (correct / total) * 100
        }, indent=2))

    def print_detail(self):
        for ref in self.references:
            self.logger.info(json.dumps(ref[3], indent=2))

    def get_logger(self):
        logger = logging.getLogger('matcher_runner')

        fh = logging.FileHandler(os.path.join(BASE_PATH, 'matcher/logs/{}_{}.log'.format(time.asctime(), self.matcher_name)), encoding='utf-8')
        fh.setLevel(logging.DEBUG)
        logger.addHandler(fh)

        sh = logging.StreamHandler(sys.stdout)
        formatter = logging.Formatter('[%(levelname)s|%(filename)s:%(lineno)s] %(asctime)s > %(message)s')
        sh.setFormatter(formatter)
        sh.setLevel(logging.INFO)
        if self.verbose:
            sh.setLevel(logging.DEBUG)
        logger.addHandler(sh)

        logger.setLevel(logging.DEBUG)

        return logger

    @staticmethod
    def read_answer(answer_path):
        with open(answer_path, 'r') as f:
            answers = json.load(f)

        answer_references = []
        for slide in answers:
            for pair in slide['pairs']:
                answer_references.append(BaseMatcher.Reference(slide['slide_id'], pair['word_id'], pair['subtitle_id']))

        return answer_references

    @staticmethod
    def evaluate(result_references, truth_references):
        count_total = len(result_references)
        count_correct = 0
        for ref in truth_references:
            for output in result_references:
                if output[:3] == ref:
                    count_correct = count_correct + 1
                # if ref in result_references:

        return count_total, count_correct


cases = [
    {
        'name': 'video1',
        'slides_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video1/video1_slides.json',
        'subtitle_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video1/video1_sub.json',
        'answer_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video1/video1_pairs.json',
    },
    {
        'name': 'video3',
        'slides_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video3/video3_slides.json',
        'subtitle_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video3/video3_subs.json',
        'answer_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video3/video3_pairs_gt.json',
    },
    {
        'name': 'video4',
        'slides_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video4/video4_slides.json',
        'subtitle_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video4/video4_subs.json',
        'answer_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video4/video4_pairs.json',
    },
    {
        'name': 'video5',
        'slides_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video5/video5_slides.json',
        'subtitle_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video5/video5_subs.json',
        'answer_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video5/video5_pairs.json',
    },
    {
        'name': 'video6',
        'slides_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video6/video6_slides.json',
        'subtitle_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video6/video6_subs.json',
        'answer_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video6/video6_pairs.json',
    },
    {
        'name': 'video7',
        'slides_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video7/video7_slides.json',
        'subtitle_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video7/video7_subs.json',
        'answer_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video7/video7_pairs.json',
    },
    {
        'name': 'video8',
        'slides_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video8/video8_slides.json',
        'subtitle_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video8/video8_subs.json',
        'answer_path': '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video8/video8_pairs.json',
    },
]

if __name__ == '__main__':
    # Syntactic matcher
    # runner_syntactic = Runner('syntactic', verbose=True)
    # runner_syntactic.run_cases(cases)
    # runner_syntactic.report_total()

    # Semantic Matcher (dup)
    # runner_semantic = Runner('semantic')
    # runner_semantic.run_cases(cases)
    # runner_semantic.report_total()

    # Semantic Syntactic Matcher
    # runner_semantic_syntactic = Runner('semantic_syntactic', verbose=True)
    # runner_semantic_syntactic.run_cases(cases)
    # runner_semantic_syntactic.report_total()

    # Semantic Ordered Matcher
    # runner_semantic_ordered = Runner('semantic_permutation', verbose=True)
    # runner_semantic_ordered.run_case(cases[-2])
    # runner_semantic_ordered.run_cases(cases[0])
    # runner_semantic_ordered.report_total()
    # runner_semantic_ordered.print_detail()

    # Permutation based Matcher
    # runner_permutation_naive = Runner('semantic_permutation', verbose=True)
    runner_permutation_naive = Runner('semantic_permutation')
    # runner_permutation_naive.run_case(cases[0])
    runner_permutation_naive.run_cases(cases)
    runner_permutation_naive.report_total()
    runner_permutation_naive.print_detail()

