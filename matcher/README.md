Video Matcher

Given slide and subtitle, compute semantic reference between them

# Usage
```bash
python run.py --method naive --slides_path video1_slides.json --subtitle_path video1_sub.json
```

Then it returns `video1_naive.jsonl` which looks like
```jsonl
{ "slide_no": 1, "word_id": 1, "subtitle_id": 3 } 
{ "slide_no": 1, "word_id": 2, "subtitle_id": 4 } 
```

