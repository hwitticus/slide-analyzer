from matcher import SemanticNaiveMatcher
from matcher import utils


class SemanticOrderedMatcher(SemanticNaiveMatcher):

    def __init__(self, slides_path, subtitle_path, logger=None):
        super().__init__(slides_path, subtitle_path)
        if logger:
            self.logger = logger

    def match(self):
        """
        Run matcher for each slide
        :return: list of tuple
        """
        references = []
        for slide in self.slides:
            slide_subtitles = self.subtitle_for_slide(slide, self.subtitles)
            new_references = self.match_slide_ordered(slide, slide_subtitles)
            references += new_references
        return references

    def match_slide_ordered(self, slide, subtitles):
        """
        Find matching subtitles ONLY after last subtitle
        :param slide: slide dict
        :param subtitles: list of subtitle dict
        :return: list of tuple
        """
        references = []
        last_subtitle_id = -1
        self.log_slide(self.logger, slide, subtitles)

        for word in slide['words']:
            remaining_subtitles = [subtitle for subtitle in subtitles if subtitle['id'] >= last_subtitle_id]
            references += self.find_most_similar_subtitle(slide['index'], word, remaining_subtitles)
            if len(references) > 0:
                last_subtitle_id = [reference[2] for reference in references][-1]
        return references

    def find_most_similar_subtitle(self, slide_id, word, subtitles):
        self.logger.debug("Slide{} Word '{}'".format(slide_id, word['text']))
        max_similarity, max_subtitle, max_subphrase = -999, None, ""

        for subtitle in subtitles:
            subtitle_text = " ".join(subtitle['lines'])
            tree = utils.make_tree(subtitle_text)
            bfs = utils.traverse_bfs(tree)
            for node in bfs:
                leaves = node.leaves()
                phrase_leaves = " ".join(leaves)
                try:
                    similarity = self.compute_phrase_similarity(word['text'], phrase_leaves)

                except self.EmptyToken as e:
                    continue
                if similarity > max_similarity:
                    max_similarity, max_subtitle, max_subphrase = similarity, subtitle, phrase_leaves
                    self.logger.debug("\t UPDATE {} {} {}".format(subtitle['id'], round(similarity, 2), phrase_leaves))
                else:
                    self.logger.debug("\t {} {} {}".format(subtitle['id'], round(similarity, 2), phrase_leaves))

        if max_subtitle is None:
            return []
        self.logger.debug("\t Chosen {} {} '{}'".format(max_subtitle['id'], round(max_similarity, 2), max_subphrase))
        detail = {
            'slide_id': slide_id,
            'subtitle_id': max_subtitle['id'],
            'max_similarity': max_similarity,
            'word_text': word['text'],
            'subtitle_text': " ".join(max_subtitle['lines']),
        }
        return [(slide_id, word['id'], max_subtitle['id'], detail)]


