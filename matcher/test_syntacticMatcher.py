from unittest import TestCase
import os

from matcher import SyntacticMatcher
BASE_PATH = os.path.abspath(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))


class TestSyntacticMatcher(TestCase):
    hello = 1
    world = 2

    @classmethod
    def setUpClass(cls):
        slides_path = os.path.join(BASE_PATH, 'parser/data/video1/video1_slides.json')
        subtitle_path = os.path.join(BASE_PATH, 'parser/data/video1/video1_sub.json')
        cls.matcher = SyntacticMatcher(slides_path, subtitle_path)

    def test_contains_basic(self):
        a = ["a", "b", "c", "d"]
        b = ["a", "b"]

        self.assertTrue(TestSyntacticMatcher.matcher.contains(a, b))
        self.assertFalse(TestSyntacticMatcher.matcher.contains(b, a))

    def test_contains_empty(self):
        matcher = TestSyntacticMatcher.matcher
        a = ["a", "b", "c", "d"]
        b = []

        self.assertFalse(matcher.contains(a, b))
        self.assertFalse(matcher.contains(b, a))

    def test_match_subtitle(self):
        matcher = TestSyntacticMatcher.matcher
        slide_id = -1
        subtitle = {
            'id': 1,
            'lines': ["So, a cryptographic hash function", "is a mathematical function."]
        }

        words_tokenized = [
            {'id': 1, 'text': 'Cryptographic Hash Functions'},
            {'id': 2, 'text': 'Mathematical function'},
        ]
        for word in words_tokenized:
            word['tokens'] = matcher.stemmize(word['text'])

        references = matcher.match_subtitle(slide_id, subtitle, words_tokenized)
        self.assertIn(matcher.Reference(-1, 1, 1), references)
        self.assertIn(matcher.Reference(-1, 2, 1), references)

    def test_match_slide(self):
        matcher = TestSyntacticMatcher.matcher

        slide = matcher.slides[0]
        subtitle = matcher.subtitle_for_slide(slide, matcher.subtitles)

        references = matcher.match_slide(slide, subtitle)
        self.assertIn(matcher.Reference(0, 3, 1), references)

    def test_match_slides(self):
        matcher = TestSyntacticMatcher.matcher
        references = matcher.match()
        print(references)
        self.fail()


