from embeddings import GloveEmbedding
from matcher import BaseMatcher
import numpy as np


class SemanticNaiveMatcher(BaseMatcher):

    def __init__(self, slides_path, subtitle_path):
        self.slides = self.open_slides(slides_path)
        self.subtitles = self.open_subtitle(subtitle_path)
        self.glove = GloveEmbedding('common_crawl_840', d_emb=300, show_progress=False)

    def match(self):
        """
        Run matcher for each slide
        :return: list of tuple
        """
        references = []
        for slide in self.slides:
            slide_subtitles = self.subtitle_for_slide(slide, self.subtitles)
            new_references = self.match_slide_allow_dup(slide, slide_subtitles)
            references += new_references

        return references

    def match_slide_allow_dup(self, slide, subtitles):
        """
        For each word in slide, find closest subtitle
        :param slide: slide dict
        :param subtitles: list of subtitle dict
        :return: list of tuple
        """
        references = []
        # Tokenize words in slide

        for word in slide['words']:
            references += self.find_most_similar_subtitle(slide['index'], word, subtitles)

        return references

    def find_most_similar_subtitle(self, slide_id, word, subtitles):
        """
        Match pair with the most closes subtitle to given word
        :param slide_id: int
        :param word: word dict
        :param subtitles: list of subtitle dict
        :return: list of tuple
        """
        max_similarity, max_subtitle_id = -999, -1
        for subtitle in subtitles:
            subtitle_text = " ".join(subtitle['lines'])
            try:
                similarity = self.compute_phrase_similarity(word['text'], subtitle_text)
            except self.EmptyToken as e:
                continue
            if similarity > max_similarity:
                max_similarity, max_subtitle_id = similarity, subtitle['id']

        if max_subtitle_id == -1:
            return []
        return [(slide_id, word['id'], max_subtitle_id)]

    """
    Functions for compute similarities
    """
    def compute_phrase_similarity(self, s1, s2):
        v1 = self.compute_sum_embedding(s1)
        v2 = self.compute_sum_embedding(s2)
        return self.compute_cos_sim(v1, v2)

    def compute_sum_embedding(self, sentence):
        tokens = self.tokenize(sentence)
        if len(tokens) is 0:
            raise self.EmptyToken(sentence)

        vectors = self.compute_embedding(tokens)
        sum_vector = np.zeros(len(vectors[0]))

        for vector in vectors:
            if vector[0] is None:
                continue
            sum_vector += vector
        return sum_vector

    def compute_embedding(self, tokens):
        vectors = [np.array(self.glove.emb(token)) for token in tokens]
        return vectors

    @staticmethod
    def compute_cos_sim(v1, v2):
        assert v1.shape == v2.shape, "Two vectors are in wrong dimension"
        return np.inner(v1, v2) / (np.linalg.norm(v1) * np.linalg.norm(v2))
