from unittest import TestCase
from matcher.utils import make_tree, traverse_bfs


class TestUtils(TestCase):

    def setUp(self):
        self.sentence = "Your father is a great teacher"

    def test_make_tree(self):
        tree = make_tree(self.sentence)
        self.assertCountEqual(tree.leaves(), self.sentence.split())

    def test_traverse_bfs(self):
        tree = make_tree(self.sentence)
        bfs_generator = traverse_bfs(tree)
        labels = []
        for node in bfs_generator:
            labels.append(node.label())

        labels_answer_partial = ['ROOT', 'S', 'NP', 'VP', 'PRP$', 'NN']
        self.assertCountEqual(labels_answer_partial, labels[:len(labels_answer_partial)])
