import jellyfish


class VideoItem:
    def __init__(self, start, end, position):
        self.start = int(start)
        self.end = int(end)
        self.position = position


class TextItem(VideoItem):
    def __init__(self, start, end, position, text):
        super().__init__(start, end, position)
        self.text = text

    def __str__(self):
        return self.text

    def check_similar(self, text_item):
        if not isinstance(text_item, TextItem): # Item should be instance of TextItems
            return False
        if jellyfish.levenshtein_distance(self.text, text_item.text) > 4:
            return False
        if self.position.get_intersection_score(text_item.position) < 0.8:
            return False
        return True

    def check_contain(self, other_textitem):
        if not isinstance(other_textitem, TextItem):
            raise Exception("Only text can contain text")
        if self.position.enclose(other_textitem.position) and other_textitem.text in self.text:
            return True
        return False

    def make_dict(self):
        return {
            'start': self.start,
            'end': self.end,
            'text': self.text,
            'position': self.position.make_dict()
        }


class OCRItem:
    def __init__(self, _id, text, position):
        self._id = _id
        self.text = text
        self.position = position

    def __str__(self):
        return self.text

    def merge(self, other):
        text = self.text + " " + other.text
        position = self.position.merge(other.position)
        return OCRItem(self._id, text, position)

    def make_dict(self):
        return {
            'text': self.text,
            'position': self.position.make_dict(),
            'id': self._id,
        }
