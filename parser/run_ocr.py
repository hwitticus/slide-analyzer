import argparse
import os
import io
import json

from tqdm import tqdm
from google.cloud import vision
vision_client = vision.Client()


def process_label(entity):
    box = dict()
    box['description'] = entity.description
    box['vertices'] = [(v.x_coordinate, v.y_coordinate) for v in entity.bounds.vertices]
    return box


def process_image(image_path):
    with io.open(image_path, 'rb') as image_file:
        content = image_file.read()
        image = vision_client.image(
            content=content)
        labels = [process_label(label) for label in image.detect_text()]
    return labels


def main(input_directory, unique_frames):
    # Create output directory if not exists
    input_path = os.path.abspath(input_directory)
    parent_path = os.path.dirname(input_path)
    output_dir = os.path.join(parent_path, 'results')
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # label images
    files = [f for f in os.listdir(input_path) if os.path.splitext(f)[1] == '.jpg']
    for index, image_file in enumerate(files):
        if index + 1 not in unique_frames:
            continue
        image_path = os.path.join(input_path, image_file)
        result = process_image(image_path)
        output_filename = os.path.splitext(image_file)[0] + '.json'
        output_path = os.path.join(output_dir, output_filename)
        with open(output_path, 'w') as f:
            json.dump(result, f)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Detects text in the images in the given directory.')
    parser.add_argument('input_directory', help='the image directory you\'d like to detect text in.')
    parser.add_argument('frames', help='one-index of unique frames', nargs='+')
    args = parser.parse_args()
    frames = list(map(int, args.frames))

    main(args.input_directory, frames)
