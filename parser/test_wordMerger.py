from unittest import TestCase
import json
from parser.merge_words import WordMerger
from parser.position import Position
from parser.item import OCRItem


class TestWordMerger(TestCase):
    def test_merge_works(self):
        input_path = '/Users/shik/Dropbox/classes/17.s/slide-analyzer/parser/data/video1/results'
        output_path = ''
        indices = [8, 17, 48, 57, 58, 66, 67, 70]

        merger = WordMerger(input_path, output_path, indices)
        merged_frames = merger.merge_frames()

    def test_is_near_position(self):
        p1 = Position(20, 20, 10, 10)
        p2 = Position(20, 35, 10, 10)
        self.assertEqual(WordMerger.is_near_position(p1, p2), True)

        p3 = Position(20, 20, 10, 10)
        p4 = Position(15, 20, 10, 15)
        self.assertEqual(WordMerger.is_near_position(p3, p4), True)

