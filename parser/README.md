# Slide video parser
A script that takes a video into input, and returns into a group of objects.

1. Make your video into a list of pictures at `frames/<video>` folder
```commandline
ffmpeg -i <path of video> -r 1/1 %04d.jpg
```

2. Analyze each frames using Cloud Vision API
```commandline
python run_ocr.py <frames_directory> [index of unique frames]
```

3. Extract unique frames based on pixel and text
```commandline
python get_slide.py <frames_directory> <results_directory>
```

4. Merge near words in slides (currently words are separated)
```commandline
python merge_words.py <input_file> 
```

5. Convert subtitle into json format
```commandline
python parse_subtitle <subtitle_path> <subtitle_json_path>
```

5. Parser's job is done. Yay :D

