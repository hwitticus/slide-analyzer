from unittest import TestCase

from parser.item import OCRItem
from parser.position import Position


class TestOCRItem(TestCase):
    def test_merge(self):
        item1 = OCRItem('1', 'hi', Position(10, 10, 10, 10))
        item2 = OCRItem('2', 'hello', Position(10, 20, 10, 10))
        merged = item1.merge(item2)
        merged_pos = merged.position
        self.assertEqual(merged._id, '1')
        self.assertEqual(merged.text, 'hi hello')
        self.assertEqual([merged_pos.top, merged_pos.left, merged_pos.width, merged_pos.height], [10, 10, 20, 10])
