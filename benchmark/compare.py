import argparse
import json
import logging.handlers

logger = logging.getLogger()
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.DEBUG)


def compare(machine_path, human_path):
    # Open machine file
    logger.debug("Loading " + machine_path)
    with open(machine_path, 'r') as f:
        machine_slides = json.load(f)
    machine_pairs = []
    for slide in machine_slides:
        for pair in slide['pairs']:
            machine_pairs.append({
                "slide": slide['slide_id'],
                "word_id": pair["word_id"],
                "subtitle_id": pair["subtitle_id"],
            })

    # Open human file
    logger.debug("Loading " + human_path)
    with open(human_path) as f:
        human_pairs = json.load(f)

    # Compare
    total_count = len(human_pairs)
    correct_count = 1
    for h in human_pairs:
        for m in machine_pairs:
            if h['slide'] == m['slide'] and h['word_id'] == m['word_id'] and h['subtitle_id'] == m['subtitle_id']:
                correct_count += 1
                break

    logger.info("Total tagged pairs " + str(total_count))
    logger.info("Matching pairs " + str(correct_count))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="machine pair vs human pair")
    parser.add_argument('machine_pair')
    parser.add_argument('human_pair')
    args = parser.parse_args()

    compare(args.machine_pair, args.human_pair)
